// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
import actions from "../actions/developer";
const pluginActions = qu4rtet.require("./actions/plugins").default;
const {handleActions} = qu4rtet.require("redux-actions");
const {setServerState} = qu4rtet.require("./lib/reducer-helper");
const {showMessage} = qu4rtet.require("./lib/message");
const {pluginRegistry} = qu4rtet.require("./plugins/pluginRegistration");
const {updateMessages} = qu4rtet.require("./reducers/locales");
const clearModule = qu4rtet.require("clear-module");

export const addDevPlugin = pluginPath => {
  return dispatch => {
    try {
      console.log("Plugin Path is", pluginPath);
      if (pluginPath) {      
	try {
          clearModule.match(new RegExp(pluginPath));
	} catch (e) {
          console.log("plugin not already installed", e);
	}
	let plugin = require(pluginPath);
	plugin.enablePlugin();
	let state = store.getState();
	dispatch({type: pluginActions.pluginListUpdated, payload: true});
	dispatch(updateMessages(state.intl.locale));
	showMessage({
          msg: "Dev plugin enabled",
          type: "success"
	});

	return dispatch({type: actions.addDevPlugin, payload: pluginPath});
      }
    } catch (e) {
      showMessage({
        msg: "An error occurred while enabling dev plugin: " + e.stack,
        type: "error"
      });
      throw new Error(e);
    }
  };
};

export const removeDevPlugin = pluginPath => {
  return dispatch => {
    return dispatch({type: actions.removeDevPlugin, payload: pluginPath});
  };
};

export default handleActions(
  {
    [actions.addDevPlugin]: (state, action) => {
      if (!state.devPlugins) {
	// quick fix for issue with error on dynamic enable.
	state.devPlugins = [];
      }
      return {
        ...state,
        devPlugins: [...state.devPlugins, action.payload]
      };
    },
    [actions.removeDevPlugin]: (state, action) => {
      if (!state.devPlugins) {
	// quick fix for issue with error on dynamic enable.
	state.devPlugins = [];
      }
      let newDevPlugins = state.devPlugins.filter(pluginPath => {
        if (pluginPath === action.payload) {
          return false;
        }
        return true;
      });
      return {
        ...state,
        devPlugins: newDevPlugins
      };
    }
  },
  {}
);
